# PaintLite

![](https://gistrec.ru/wp-content/uploads/2020/03/PaintLite.png)

## Вклад участников в проект

* Ковалько Александр - 20%
* Головко Илья - 20%
* Космынина Марина - 15%
* Космынин Андрей - 15%
* Сорокин Сергей - 10%
* Кононова Александра - 10%
* Попов Александр - 10%
* Жигалов Константин - 10%

## Команды

#### Документация и консультация
Изучение WPF, ReactiveUI, System.Composition и создание документации с инструкциями и ответами на распространенные вопросы.
* Космынин Андрей (100%)

---

#### Графический интерфейс
Создание графического интерфейса, логотипа и всё что связано с UI.  
Исправление ошибок, связанных с xaml разметкой.
* Космынина Марина (70%)
* Попов Александр (30%)

---

#### Фигуры
Разработка основных интерфейсов, разработка классов фигур, их дескрипторов и функций для работы с ними.  
Создание класса для рисования фигур.
* Головко Илья (60%)
* Сорокин Сергей (40%)

---

#### Логика
Разработка основной логики: выбор активной фигуры, толщины линии, цвета.  
Создание функций-хэндлеров для UI.
* Кононова Александра (60%)
* Жигалов Константин (40%)

---

#### Лидер проекта
Создание ТЗ, проекта, репозитория, инструкций по работе с git.  Рефакторинг кода.  
Консультирование по вопросам git. Исправление merge конфликтов.  
* Ковалько Александр (100%)