﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Media;

namespace Painter
{
    public class Graphic : Editor.IGraphic
    {
        Canvas canvas;

        public Graphic(Canvas canvas)
        {
            this.canvas = canvas;
        }

        public void Polyline(IEnumerable<Editor.Point> points, Editor.Color color, uint thickness)
        {
            Polyline polyline = new Polyline();

            polyline.Stroke = new SolidColorBrush(Color.FromRgb(color.R, color.G, color.B));
            polyline.StrokeThickness = thickness;

            foreach (var point in points)
            {
                polyline.Points.Add(new System.Windows.Point(point.X, point.Y));
            }

            canvas.Children.Add(polyline);
        }

        public void Polygon(IEnumerable<Editor.Point> points, Editor.Color color, uint thickness)
        {
            Polygon polygon = new Polygon();

            polygon.Stroke = new SolidColorBrush(Color.FromRgb(color.R, color.G, color.B));
            polygon.StrokeThickness = thickness;

            foreach (var point in points)
            {
                polygon.Points.Add(new System.Windows.Point(point.X, point.Y));
            }

            canvas.Children.Add(polygon);
        }

        public void Circle(Editor.Point A, double radius, Editor.Color color, uint thickness)
        {
            Ellipse myEllipse = new Ellipse();

            myEllipse.Stroke = new SolidColorBrush(Color.FromRgb(color.R, color.G, color.B));
            myEllipse.StrokeThickness = thickness;

            myEllipse.Width = 2 * radius;
            myEllipse.Height = 2 * radius;

            canvas.Children.Add(myEllipse);

            Canvas.SetLeft(myEllipse, A.X - radius);
            Canvas.SetTop(myEllipse, A.Y - radius);
        }
    }
}
