﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Editor;

namespace WpfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IViewFor<ViewModel.ViewModel>, IReactiveObject
    {
        public ReactiveCommand<Unit, Unit> Delete { get; set; }

        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();

            this.WhenActivated(disposer =>
            {
                Delete = ReactiveCommand.Create<Unit, Unit>(_ =>
                {
                    MyCanvas.Children.Clear();
                    ViewModel.ClearFigures();

                    return default;
                }).DisposeWith(disposer);
                this.RaisePropertyChanged("Delete");
            });

            graphic = new Painter.Graphic(MyCanvas);

            SelectedFigure = Circle;
            SelectedColor  = DefaultColor;
            SelectedThickness = DefaultThickness;
        }
        ViewModel.ViewModel viewModel = new ViewModel.ViewModel();
        public ViewModel.ViewModel ViewModel { get => viewModel; set { } }
        object IViewFor.ViewModel { get => ViewModel; set => ViewModel = (ViewModel.ViewModel)value; }

        public void RaisePropertyChanging(PropertyChangingEventArgs args)
        {
            PropertyChanging.Invoke(this, args);
        }

        public void RaisePropertyChanged(PropertyChangedEventArgs args)
        {
            PropertyChanged.Invoke(this, args);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangingEventHandler PropertyChanging;

        private Button SelectedFigure;
        private Button SelectedColor;
        private Button SelectedThickness;
        private List<Editor.Point> points = new List<Editor.Point>();

        private Editor.IGraphic graphic;

        private void onFigureSelect(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            SelectedFigure.BorderThickness = new Thickness(0.0);

            button.BorderThickness = new Thickness(3.0);
            SelectedFigure = button;

            points.Clear();
        }

        private void onColorSelect(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            SelectedColor.BorderThickness = new Thickness(0.0);

            button.BorderThickness = new Thickness(3.0);
            SelectedColor = button;
        }

        private void onThicknessSelect(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            SelectedThickness.BorderThickness = new Thickness(0.0);

            button.BorderThickness = new Thickness(3.0);
            SelectedThickness = button;
        }

        /**
         * Функция вызывается при клике по Canvas'у
         * В этом случае добавляем точку в массив точек
         * И проверяем, можно ли создать выбранную фигуну (т.е. хватает ли точек в массиве)
         * Если точек достаточно, то создаем фигуру и добавляем
         */
        private void onCanvasClick(object sender, EventArgs e)
        {
            Editor.Point point = new Editor.Point(Mouse.GetPosition(MyCanvas).X, Mouse.GetPosition(MyCanvas).Y);
            points.Add(point);

            // Если достаточно точек для создания фигуры, создаем
            if (points.Count() == viewModel.NumberOfPoints(SelectedFigure.Name))
            {
                IFigure figure = viewModel.Create(SelectedFigure.Name, points);

                var color = ((SolidColorBrush)SelectedColor.Background).Color;

                figure.color = new Editor.Color(color.R, color.G, color.B);
                figure.thickness = UInt32.Parse(SelectedThickness.DataContext.ToString());

                viewModel.Add.Execute(figure).Subscribe();
                viewModel.Draw.Execute(graphic).Subscribe();

                points.Clear();
            }
        }
    }
}
