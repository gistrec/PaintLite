﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Drawing;

namespace Editor
{
    public interface IGraphic
    {
        void Polyline(IEnumerable<Point> points, Color color, uint thickness);
        void Polygon(IEnumerable<Point> points, Color color, uint thickness);
        void Circle(Point A, double radius, Color color, uint thickness);
    }

    public struct Point
    {
        public double X;
        public double Y;

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
    public struct Color
    {
        public byte R, G, B;

        public Color(byte r, byte g, byte b)
        {
            R = r;
            G = g;
            B = b;
        }
    }

    public interface IFigure
    {
        string Name { get; }
        Color color { get; set; }
        uint thickness { get; set; }
        IEnumerable<string> Parameters { get; }
        object this[string name] { get; set; }
        void Draw(IGraphic graphic);
    }

    public interface IFigureDescriptor
    {
        string Name { get; }
        int NumberOfPoints { get; }
        IFigure Create(IEnumerable<Point> vertex);
    }

     public class FigureDescriptorMetadata
     {
         public string Name { get; set; }
     }
}
