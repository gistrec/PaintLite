﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Composition;
using System.Linq;

namespace Editor
{
    public class Triangle : IFigure
    {
        public string Name => "Triangle";

        public Color color { get; set; }
        public uint thickness { get; set; }

        Point A;
        Point B;
        Point C;

        public Triangle(Point a, Point b, Point c)
        {
            A = a;
            B = b;
            C = c;
        }

        public IEnumerable<string> Parameters { get; } = new[] { "A", "B","C" };

        public object this[string name]
        {
            get
            {
                switch (name)
                {
                    case "A": return A;
                    case "B": return B;
                    case "C": return C;
                    default: throw new ArgumentOutOfRangeException($"Unknown parameter {name}");
                }

            }
            set
            {
                if (value is Point p)
                {
                    switch (name)
                    {
                        case "A": A = p; break;
                        case "B": B = p; break;
                        case "C": C = p; break;
                        default: throw new ArgumentOutOfRangeException($"Unknown parameter {name}");
                    }
                }
                else throw new ArgumentOutOfRangeException($"Unknown parameter type {value.GetType().Name}");
            }
        }

        public void Draw(IGraphic graphic)
        {
            graphic.Polygon(new[] { A, B, C }, color, thickness);
        }
    }

    [Export(typeof(IFigureDescriptor))]
    [ExportMetadata("Name", "Triangle")]
    public class TriangleDescriptor : IFigureDescriptor
    {
        public string Name => "Triangle";
        public int NumberOfPoints => 3;

        public IFigure Create(IEnumerable<Point> vertex)
        {
            var points = vertex.ToArray();
            if (points.Length != 3) throw new ArgumentOutOfRangeException($"Bad number of parameters {points.Length}");
            return new Triangle(points[0], points[1], points[2]);
        }
    }
}
