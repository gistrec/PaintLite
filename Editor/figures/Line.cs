﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Composition;
using System.Linq;

namespace Editor
{
    public class Line : IFigure
    {
        public string Name => "Line";

        public Color color { get; set; }
        public uint thickness { get; set; }

        Point A;
        Point B;

        public Line(Point a, Point b)
        {
            A = a;
            B = b;
        }

        public IEnumerable<string> Parameters { get; } = new[] { "A", "B" };

        public object this[string name]
        {
            get
            {
                switch (name)
                {
                    case "A": return A;
                    case "B": return B;
                    default: throw new ArgumentOutOfRangeException($"Unknown parameter {name}");
                }

            }
            set
            {
                if (value is Point p)
                {
                    switch (name)
                    {
                        case "A": A = p; break;
                        case "B": B = p; break;
                        default: throw new ArgumentOutOfRangeException($"Unknown parameter {name}");
                    }
                }
                else throw new ArgumentOutOfRangeException($"Unknown parameter type {value.GetType().Name}");
            }
        }

        public void Draw(IGraphic graphic)
        {
            graphic.Polyline(new[] { A, B }, color, thickness);
        }
    }

    [Export(typeof(IFigureDescriptor))]
    [ExportMetadata("Name", "Line")]
    public class LineDescriptor : IFigureDescriptor
    {
        public string Name => "Line";
        public int NumberOfPoints => 2;

        public IFigure Create(IEnumerable<Point> vertex)
        {
            var points = vertex.ToArray();
            if (points.Length != 2) throw new ArgumentOutOfRangeException($"Bad number of parameters {points.Length}");
            return new Line(points[0], points[1]);
        }
    }
}
