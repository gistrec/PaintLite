﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Composition;
using System.Linq;

namespace Editor
{
    public class Circle : IFigure
    {
        public string Name => "Circle";

        public Color color { get; set; }
        public uint thickness { get; set; }

        public Circle(Point a, double radius)
        {
            Center = a;
            Radius = radius;
        }
        Point Center;
        double Radius;
        public IEnumerable<string> Parameters { get; } = new[] { "Center", "Radius" };

        public object this[string name]
        {
            get
            {
                switch (name)
                {
                    case "Center": return Center;
                    case "Radius": return Radius;
                    default: throw new ArgumentOutOfRangeException($"Unknown parameter {name}");
                }

            }
            set
            {
                if (name == "Center" && value is Point p)
                {
                    Center = p;
                }

                if (name == "Radius" && value is uint r)
                {
                    Radius = r;
                }
                else throw new ArgumentOutOfRangeException($"Unknown parameter type {value.GetType().Name}");
            }
        }

        public void Draw(IGraphic graphic)
        {
            graphic.Circle(Center, Radius, color, thickness);
        }
    }

    [Export(typeof(IFigureDescriptor))]
    [ExportMetadata("Name", "Circle")]
    public class CircleDescriptor : IFigureDescriptor
    {
        public string Name => "Circle";
        public int NumberOfPoints => 2;

        public IFigure Create(IEnumerable<Point> vertex)
        {
            var points = vertex.ToArray();

            double radius = Math.Sqrt((points[1].X - points[0].X) * (points[1].X - points[0].X) + (points[1].Y - points[0].Y) * (points[1].Y - points[0].Y));

            if (points.Length != 2) throw new ArgumentOutOfRangeException($"Bad number of parameters {points.Length}");
            return new Circle(points[0], radius);
        }
    }
}
