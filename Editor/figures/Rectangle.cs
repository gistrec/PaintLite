﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Composition;
using System.Linq;
using System.Windows;

namespace Editor
{
    public class Rectangle : IFigure
    {
        public string Name => "Rectangle";

        public Color color { get; set; }
        public uint thickness { get; set; }

        Point A;
        Point B;

        public Rectangle(Point a, Point b) //а - левая верхняя точка b - правая нижняя 
        {
            A = a;
            B = b;
        }

        public IEnumerable<string> Parameters { get; } = new[] { "A", "B" };

        public object this[string name]
        {
            get
            {
                switch (name)
                {
                    case "A": return A;
                    case "B": return B;
                    default: throw new ArgumentOutOfRangeException($"Unknown parameter {name}");
                }

            }
            set
            {
                if (value is Point p)
                {
                    switch (name)
                    {
                        case "A": A = p; break;
                        case "B": B = p; break;
                        default: throw new ArgumentOutOfRangeException($"Unknown parameter {name}");
                    }
                }
                else throw new ArgumentOutOfRangeException($"Unknown parameter type {value.GetType().Name}");
            }
        }

        public void Draw(IGraphic graphic)
        {
            Point point1 = new Point(A.X, A.Y);
            Point point2 = new Point(B.X, A.Y);
            Point point3 = new Point(B.X, B.Y);
            Point point4 = new Point(A.X, B.Y);

            graphic.Polygon(new[] { point1, point2, point3, point4 }, color, thickness);
        }
    }

    [Export(typeof(IFigureDescriptor))]
    [ExportMetadata("Name", "Rectangle")]
    public class RectDescriptor : IFigureDescriptor
    {
        public string Name => "Rectangle";
        public int NumberOfPoints => 2;

        public IFigure Create(IEnumerable<Point> vertex)
        {
            var points = vertex.ToArray();
            if (points.Length != 2) throw new ArgumentOutOfRangeException($"Bad number of parameters {points.Length}");
            return new Rectangle(points[0], points[1]);
        }
    }
}
